# FYEO - For your eyes only

ActiveRecord extension to manage visibility for every model. [It's for your eyes only!](http://www.youtube.com/watch?v=mnfewT7RQ0s)

## Installation

Add this line to your application's Gemfile:

    gem 'fyeo'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install fyeo

Now, run the following migration to create the needed table.

    class CreateAccessorsTable < ActiveRecord::Migration
      def self.up
        create_table :accessors, :force => true do |t|
          t.string :obj_type, :limit => 20, :null => false
          t.integer :obj_id, :null => false
          t.integer :contact_id, :null => false
        end

        add_index :accessors, [ :obj_type, :obj_id ]
        add_index :accessors, [ :contact_id ]
      end

      def self.down
        drop_table :accessors
      end
    end


## Usage

    class Document < ActiveRecord::Base
      with_accessor_control
    end

    Document.create! :title => 'For everybody'
    Document.create! :title => 'Top secret', :accessors => 1

    Document.accessable_for(1).map(&:title)
    # => [ 'For everybody', 'Top secret' ]

    Document.accessable_for(2).map(&:title)
    # => [ 'For everybody' ]


## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
