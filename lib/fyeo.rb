require 'fyeo/version'
require 'app/models/accessor'

ActiveRecord::Base.class_eval do
  def self.acts_as_accessor(options={})
    has_many :accessables, **{ :class_name => 'Accessor', :foreign_key => 'contact_id' }.merge(options)

    scope :with_access_to, lambda { |object| joins("JOIN accessors ON accessors.contact_id  = #{self.table_name}.contact_id
                                                                  AND accessors.obj_type = '#{object.class.base_class.name}'
                                                                  AND accessors.obj_id   = #{object.id}") }
  end

  def self.with_accessor_control(options={})
    class_eval do
      require 'enumerator'

      has_many :accessors, :as => :obj, :dependent => :delete_all, :inverse_of => :obj

      after_save :update_accessors

      scope :fyeo, lambda { |contact_id|
        joins("LEFT JOIN accessors ON (accessors.obj_id = #{self.table_name}.id AND accessors.obj_type = '#{self.name}')").
        where([ "accessors.contact_id IS NULL OR accessors.contact_id = ?", contact_id ]).
        readonly(false)
      }

      scope :strict_fyeo, lambda { |contact_ids|
        readonly(false).
        joins(Array(contact_ids).each_with_index.map { |contact_id, index|
                    "JOIN accessors ac_#{index} ON ac_#{index}.obj_id = #{self.table_name}.id
                                               AND ac_#{index}.obj_type = '#{self.name}'
                                               AND ac_#{index}.contact_id = #{contact_id} " })
       }

      if defined? Authorization
        # Modify declarative_authorization to include the additional scope
        class << self
          alias :old1_with_permissions_to :with_permissions_to
        end

        self.metaclass.send :define_method, :with_permissions_to do |*args|
          result = old1_with_permissions_to(*args)

          if Authorization.current_user.respond_to?(:contact_id)
            if options[:strict]
              result.strict_fyeo(Authorization.current_user.contact_id)
            else
              result.fyeo(Authorization.current_user.contact_id)
            end
          else
            result
          end
        end
      end

      if klass_names = options[:include]
        klass_names.each do |klass_name|
          klass = klass_name.to_s.pluralize.classify.constantize
          origin = self

          scope_name = "strict_fyeo_with_#{origin.table_name.singularize}".to_sym

          klass.define_singleton_method scope_name do |contact_ids|
            readonly(false).
            joins(Array(contact_ids).each_with_index.map { |contact_id, index|
              "JOIN accessors ac_#{index} ON ac_#{index}.obj_id = #{klass.table_name}.#{origin.table_name.singularize}_id
                                         AND ac_#{index}.obj_type = '#{origin.name}'
                                         AND ac_#{index}.contact_id = #{contact_id} "})
          end

          if defined? Authorization
            # Modify declarative_authorization to include the additional scope
            class << klass
              alias :old2_with_permissions_to :with_permissions_to
            end

            klass.metaclass.send :define_method, :with_permissions_to do |*args|
              result = old2_with_permissions_to(*args)

              if Authorization.current_user.respond_to?(:contact_id)
                if options[:strict]
                  result.send(scope_name, Authorization.current_user.contact_id)
                else
                  raise
                end
              else
                result
              end
            end
          end
        end
      end

      def self.reset_accessors
        self.transaction do
          Accessor.where(:obj_type => self.name).delete_all
        end
      end

      def accessor_ids
        @accessor_ids ||= self.accessors.map(&:contact_id)
      end

      def accessor_ids=(value)
        if value.blank?
          value = [ ]
        else
          value = [ value ] unless value.is_a?(Array)
          value = value.map(&:to_i)
        end

        @accessor_ids = value
      end

      def update_accessors
        return unless defined? @accessor_ids

        if @accessor_ids.length == self.accessors.length && @accessor_ids.length == 1
          # No need to delete, just update the existing one
          self.accessors.first.update! :contact_id => @accessor_ids.first
        else
          self.accessors.delete_all
          @accessor_ids.each do |v|
            self.accessors.create! :contact_id => v, :account_id => self.account_id
          end
        end
      end
    end
  end
end
