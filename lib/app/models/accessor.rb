class Accessor < ActiveRecord::Base
  belongs_to :obj, :polymorphic => true

  scope :by_obj_type, lambda { |obj_type| where(:obj_type => obj_type).includes(:obj) }
end
