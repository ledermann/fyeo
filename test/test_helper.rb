require 'minitest/autorun'
require 'active_record'
require 'fyeo'

configs = YAML.load_file(File.dirname(__FILE__) + '/database.yml')
ActiveRecord::Base.configurations = configs

ActiveRecord::Base.establish_connection(:sqlite)
ActiveRecord::Migration.verbose = false
load(File.dirname(__FILE__) + '/schema.rb')

puts "Testing with ActiveRecord #{ActiveRecord::VERSION::STRING}"

class Discussion < ActiveRecord::Base
  belongs_to :folder
end

class Folder < ActiveRecord::Base
  with_accessor_control :strict => true, :include => [:discussions]
  has_many :discussions
end

class Document < ActiveRecord::Base
  with_accessor_control
end

class User < ActiveRecord::Base
  acts_as_accessor
end
