ActiveRecord::Schema.define(:version => 1) do
  create_table :folders, :force => true do |t|
    t.column :name, :string
    t.integer :account_id
  end
  
  create_table :documents, :force => true do |t|
    t.column :name, :string
    t.integer :account_id
  end
  
  create_table :discussions, :force => true do |t|
    t.column :subject, :string
    t.column :folder_id, :string
    t.integer :account_id
  end
  
  create_table :accessors, :force => true do |t|
    t.string :obj_type, :limit => 20, :null => false
    t.integer :obj_id, :null => false
    t.integer :contact_id, :null => false
    t.integer :account_id
  end
  
  create_table :users, :force => true do |t|
    t.string :username, :null => false
    t.integer :contact_id, :null => false
    t.integer :account_id
  end
end