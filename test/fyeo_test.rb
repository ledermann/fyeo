require 'test_helper'

class FyeoTest < Minitest::Test
  def setup
  end

  def teardown
    Folder.delete_all
  end

  def test_user_with_access_to_folder
    user1 = User.create! :username => 'Mr. Foo', :contact_id => 1
    User.create! :username => 'Mr. Bar', :contact_id => 2

    folder = Folder.create! :accessor_ids => [user1.id]
    assert_equal [user1], User.with_access_to(folder)
  end

  def test_create_without_accessor_ids
    document = Document.create!
    assert_equal [ ], document.accessor_ids
  end

  def test_create_with_blank_accessor_ids
    document = Document.create! :accessor_ids => []
    assert_equal [ ], document.accessor_ids
  end

  def test_create_with_accessor_ids
    document = Document.create! :accessor_ids => 123
    assert_equal [ 123 ], document.accessor_ids
  end

  def test_create_with_accessor_ids_as_string
    document = Document.create! :accessor_ids => '123'
    assert_equal [ 123 ], document.accessor_ids
  end

  def test_update_single
    document = Document.create!
    document.update! :accessor_ids => 5

    assert_equal [ 5 ], document.accessor_ids
  end

  def test_update_multiple
    document = Document.create!
    document.update! :accessor_ids => [1,2,3]

    assert_equal [ 1,2,3 ], document.accessor_ids
  end

  def test_update_remove
    document = Document.create! :accessor_ids => [1,2,3]
    document.update! :accessor_ids => 2

    assert_equal [ 2 ], document.accessor_ids
  end

  def test_update_blank_string
    document = Document.create! :accessor_ids => 2
    document.update! :accessor_ids => ''

    assert_equal [ ], document.accessor_ids
  end

  def test_preserve_accessors_on_update
    document = Document.create! :accessor_ids => 2

    document = Document.find(document.id)
    document.update! :name => 'foo'

    assert_equal [ 2 ], document.accessor_ids
  end

  def test_assignment
    document = Document.new
    document.accessor_ids = 5
    assert_equal [ 5 ], document.accessor_ids
  end

  def test_accessable_for
    folder1 = Folder.create! :accessor_ids => 42
    folder2 = Folder.create!

    assert_equal [folder1, folder2], Folder.fyeo(42)
    assert_equal [folder2], Folder.fyeo(0)
  end

  def test_strict_accessable_for
    folder1 = Folder.create! :accessor_ids => 42
    Folder.create!

    assert_equal [folder1], Folder.strict_fyeo(42)
    assert_equal [], Folder.strict_fyeo(0)
  end

  def test_strict_accessable_for_with_include
    folder = Folder.create! :accessor_ids => [42,43]
    discussion = folder.discussions.create!

    assert_equal [discussion], Discussion.strict_fyeo_with_folder(42)
    assert_equal [discussion], Discussion.strict_fyeo_with_folder([42,43])
    assert_equal [discussion], Discussion.strict_fyeo_with_folder([43])
    assert_equal [], Discussion.strict_fyeo_with_folder(99)
  end
end
